WATCOM ?= ~/opt/watcom-v2
WPATH = $(WATCOM)/binl:$(WATCOM)/binw
INCLUDE = $(WATCOM)/h

AS = wasm
CC = wcc386
LD = wlink
DOS = dosbox

BIN_DIR := bin
SRC_DIR := src

MODULES := lib

BINS := $(patsubst $(SRC_DIR)/%.c,$(BIN_DIR)/%.exe,$(wildcard $(SRC_DIR)/*.c))
OBJS := $(addsuffix .o,$(patsubst $(SRC_DIR)/%,$(BIN_DIR)/%,$(basename \
 $(foreach s,$(addprefix $(SRC_DIR)/,$(MODULES)),$(wildcard $s/*.c $s/*asm)))))
INCLUDES := $(foreach i,$(addprefix $(SRC_DIR)/,$(MODULES)),-i$i)

PROG := $(BIN_DIR)/shdrtool.exe

LIBS := -lm

ASFLAGS := -q -fp3 -fpi87 -bt=dos
CFLAGS := -4 -fpi87 -fp3 -obe9hmil+t -wx -s -d0 -q -zm -zro -bt=dos
# Disable 'Symbol defined but not referenced' warnings
CFLAGS += -wcd202
CFLAGS += -fr=/dev/null
CFLAGS += -i$(INCLUDE) $(INCLUDES)
LDFLAGS := option map,eliminate,quiet system pmodew
#DBFLAGS := -machine svga_et4000

EMPTY :=
SPACE := $(EMPTY) $(EMPTY)
COMMA := ,

ifeq (run, $(firstword $(MAKECMDGOALS)))
  ifneq (1, $(words $(MAKECMDGOALS)))
    PROG := $(BIN_DIR)/$(word 2, $(MAKECMDGOALS)).exe
  endif
endif

guard=@mkdir -p $(@D)

all: $(OBJS) $(BINS)

$(BIN_DIR)/%.o: $(SRC_DIR)/%.asm
	$(guard)
	PATH=$(WPATH) $(AS) -fo=$@ $(ASFLAGS) $<

$(BIN_DIR)/%.o: $(SRC_DIR)/%.c $(GEN_H)
	$(guard)
	PATH=$(WPATH) INCLUDE=$(INCLUDE) $(CC) -fo=$@ $(CFLAGS) $<

$(BIN_DIR)/%.exe: $(BIN_DIR)/%.o $(OBJS)
	$(guard)
	PATH=$(WPATH) WATCOM=$(WATCOM) $(LD) name $@ $(LDFLAGS) file $(subst $(SPACE),$(COMMA),$^)
	@mv $*.map $(BIN_DIR)
#	upx $@

clean:
	$(RM) -rf $(BIN_DIR)

run: $(PROG)
	$(DOS) $(PROG) $(DBFLAGS)
