#include <dos.h>
#include <stdint.h>
#include <string.h>
#include "dpmi.h"

int DPMI_alloc_sel() {
  union REGS r;
  r.w.ax = 0x000;  /* DPMI allocate selector */
  r.w.cx = 1;
  int386(0x31, &r, &r);
  if (r.w.cflag) {
    return 0;
  }
  return r.w.ax;
}

int DPMI_free_sel(int sel) {
  union REGS r;
  r.w.ax = 0x100;  /* DPMI free selector */
  r.w.bx = sel;
  int386(0x31, &r, &r);
  if (r.w.cflag) {
    return 0;
  }
  return 1;
}

int DPMI_set_selector_rights(int sel, int access) {
  union REGS r;
  r.w.ax = 0x009;  /* DPMI set access rights */
  r.w.bx = sel;
  r.w.cx = access; /* 32-bit page granular */
  int386(0x31, &r, &r);
  if (r.w.cflag) {
    return 0;
  }
  return 1;
}

int DPMI_map_physical_address(unsigned int addr, unsigned int limit) {
  union REGS r;
  r.w.ax = 0x800;
  r.w.bx = addr >> 16;
  r.w.cx = addr & 0xffff;
  r.w.si = limit >> 16;
  r.w.di = limit & 0xffff;
  int386(0x31, &r, &r);
  if (r.w.cflag) {
    return 0;
  }
  return ((int)r.w.bx << 16) + r.w.cx;
}

int DPMI_set_selector_base(int sel, unsigned int addr) {
  union REGS r;
  r.w.ax = 0x007;
  r.w.bx = sel;
  r.w.cx = addr >> 16;
  r.w.dx = addr & 0xffff;
  int386(0x31, &r, &r);
  if (r.w.cflag) {
    return 0;
  }
  return 1;
}

int DPMI_set_selector_limit(int sel, unsigned int limit) {
  union REGS r;
  r.w.ax = 0x008;
  r.w.bx = sel;
  r.w.cx = limit >> 16;
  r.w.dx = limit & 0xffff;
  int386(0x31, &r, &r);
  if (r.w.cflag) {
    return 0;
  }
  return 1;
}

int DPMI_alloc(int len, int *sel, int *seg) {
  union REGS r;
  r.w.ax = 0x100;
  r.w.bx = (len + 0xf) >> 4;
  int386(0x31, &r, &r);
  if (r.w.cflag) {
    return 0;
  }
  *sel = r.w.dx;
  *seg = r.w.ax;
  return 1;
}

void DPMI_free(int sel) {
  union REGS r;
  r.w.ax = 0x101;
  r.w.dx = sel;
  int386(0x31, &r, &r);
}

void DPMI_int86(int num, RMREGS *regs) {
  union REGS r;
  struct SREGS sr;
  segread(&sr);
  r.w.ax = 0x300;
  r.h.bl = num;
  r.h.bh = 0;
  r.w.cx = 0;
  sr.es = sr.ds;
  r.x.edi = (unsigned)regs;
  int386x(0x31, &r, &r, &sr);
}
