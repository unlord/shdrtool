#if !defined(_dpmi_H)
# define _dpmi_H (1)

#define DOS_OFF(p) (((unsigned int)(p)) & 0Xffff)
#define DOS_SEG(p) (((unsigned int)(p)) >> 16)
#define CONV_RM_TO_PM(p) ((void *)((DOS_SEG(p) << 4) + DOS_OFF(p)))

#undef __FILLER
#define __FILLER(a) unsigned short a;

struct DWORDRMREGS {
  unsigned int edi;
  unsigned int esi;
  unsigned int ebp;
  unsigned int reserved;
  unsigned int ebx;
  unsigned int edx;
  unsigned int ecx;
  unsigned int eax;
};

struct WORDRMREGS {
  unsigned short di; __FILLER(_1);
  unsigned short si; __FILLER(_2);
  unsigned short bp; __FILLER(_3);
  unsigned short reserved; __FILLER(_4);
  unsigned short bx; __FILLER(_5);
  unsigned short dx; __FILLER(_6);
  unsigned short cx; __FILLER(_7);
  unsigned short ax; __FILLER(_8);
};

struct BYTERMREGS {
  __FILLER(_1); __FILLER(_2);
  __FILLER(_3); __FILLER(_4);
  __FILLER(_5); __FILLER(_6);
  __FILLER(_7); __FILLER(_8);
  unsigned char bl, bh; __FILLER(_9);
  unsigned char dl, dh; __FILLER(_10);
  unsigned char cl, ch; __FILLER(_11);
  unsigned char al, ah; __FILLER(_12);
};

struct RMREGS {
  union {
    struct DWORDRMREGS x;
    struct WORDRMREGS w;
    struct BYTERMREGS h;
  };
  unsigned short flags;
  unsigned short es;
  unsigned short ds;
  unsigned short fs;
  unsigned short gs;
  unsigned short ip;
  unsigned short cs;
  unsigned short sp;
  unsigned short ss;
};

typedef struct RMREGS RMREGS;

int DPMI_alloc_sel();
int DPMI_free_sel(int sel);
int DPMI_set_selector_rights(int sel, int access);
int DPMI_map_physical_address(unsigned int addr, unsigned int limit);
int DPMI_set_selector_base(int sel, unsigned int addr);
int DPMI_set_selector_limit(int sel, unsigned int limit);
int DPMI_alloc(int len, int *sel, int *seg);
void DPMI_free(int sel);
void DPMI_int86(int num, RMREGS *regs);

#endif
