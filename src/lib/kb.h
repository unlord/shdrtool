#if !defined(_dpmi_H)
# define _dpmi_H (1)

static char KB_keypress();
#pragma aux KB_keypress = \
  "in al, 0x60" \
  modify [ al ] \
  value [ al ]

#define KB_esc() (KB_keypress() == 0x1)

#endif
