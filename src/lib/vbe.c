#include <stdlib.h>
#include <string.h>
#include <dos.h>
#include "dpmi.h"
#include "vbe.h"

static VBEInfo vbe_info;
static VBEPmodeFuncs vbe_pmode_funcs;
static VBEModeInfo vbe_mode_info;

static int VBE2_set_window(int addr);
#pragma aux VBE2_set_window = \
  "mov eax, 0x4f05" \
  "xor ebx, ebx" \
  "call dword ptr [vbe_pmode_funcs + 0]" \
  parm [ edx ] \
  modify [ ebx ] \
  value [ eax ]

static int VBE2_set_display_start(int offset);
#pragma aux VBE2_set_display_start = \
  "mov eax, 0x4f07" \
  "mov bx, 0x80" \
  "shr ecx, 2" \
  "mov edx, ecx" \
  "shr edx, 16" \
  "call dword ptr [vbe_pmode_funcs + 4]" \
  parm [ ecx ] \
  modify [ ebx edx ] \
  value [ eax ]

static int VBE2_set_palette(const VBEColor *colors, short start, short num);
#pragma aux VBE2_set_palette = \
  "mov eax, 0x4f09" \
  "mov bx, 0" \
  "call dword ptr [vbe_pmode_funcs + 8]" \
  parm [ edi ] [ edx ] [ ecx ] \
  modify [ ebx ] \
  value [ eax ]

int VBE_set_window(int addr) {
  if (vbe_pmode_funcs.set_window_func != NULL) {
    return VBE2_set_window(addr);
  }
  else {
    RMREGS r;
    memset(&r, 0, sizeof(r));
    r.w.ax = 0x4f05;
    r.w.bx = 0;
    r.w.dx = addr;
    DPMI_int86(0x10, &r);
    return r.w.ax == 0x4f;
  }
}

int VBE_set_display_start(int row, int col) {
  if (vbe_pmode_funcs.set_display_start_func != NULL) {
    return VBE2_set_display_start(vbe_mode_info.bytes_per_scanline*row + col);
  }
  else {
    RMREGS r;
    memset(&r, 0, sizeof(r));
    r.w.ax = 0x4f07;
    r.w.bx = 0x80;
    r.w.cx = col;
    r.w.dx = row;
    DPMI_int86(0x10, &r);
    return r.w.ax == 0x4f;
  }
}

int VBE_set_palette(const VBEColor *colors, short start, short num) {
  if (vbe_pmode_funcs.set_palette_func != NULL) {
    return VBE2_set_palette(colors, start, num);
  }
  else {
    RMREGS r;
    memset(&r, 0, sizeof(r));
    r.w.ax = 0x4f09;
    r.w.bx = 0;
    r.w.cx = num;
    r.w.dx = start;
    r.x.edi = (unsigned int)colors;
    DPMI_int86(0x10, &r);
    return r.w.ax == 0x4f;
  }
}

static int VBE_buffer_call(RMREGS *r, void *buf, int size) {
  int sel;
  int seg;
  if (!DPMI_alloc(size, &sel, &seg)) {
    return 0;
  }
  r->es = seg;
  r->w.di = 0;
  _fmemcpy(MK_FP(sel, 0), buf, size);
  DPMI_int86(0x10, r);
  if (r->w.ax == 0x4f) {
    _fmemcpy(buf, MK_FP(sel, 0), size);
  }
  DPMI_free(sel);
  return r->w.ax == 0x4f;
}

int VBE_get_info(VBEInfo *vbe_info) {
  RMREGS r;
  memset(&r, 0, sizeof(r));
  r.w.ax = 0x4f00;
  strncpy(vbe_info->vbe_signature, "VBE2", 4);
  if (!VBE_buffer_call(&r, vbe_info, sizeof(*vbe_info))) {
    return 0;
  }
  if (strncmp(vbe_info->vbe_signature, "VESA", 4) != 0) {
    return 0;
  }
  /* Convert the struct pointers for use in protected mode. */
  vbe_info->oem_string_ptr = CONV_RM_TO_PM(vbe_info->oem_string_ptr);
  vbe_info->video_mode_ptr = CONV_RM_TO_PM(vbe_info->video_mode_ptr);
  vbe_info->oem_vendor_name_ptr = CONV_RM_TO_PM(vbe_info->oem_vendor_name_ptr);
  vbe_info->oem_product_name_ptr =
   CONV_RM_TO_PM(vbe_info->oem_product_name_ptr);
  vbe_info->oem_product_rev_ptr = CONV_RM_TO_PM(vbe_info->oem_product_rev_ptr);
  return r.w.ax == 0x4f;
}

int VBE_get_mode_info(VBEModeInfo *vbe_mode_info, int mode) {
  RMREGS r;
  memset(&r, 0, sizeof(r));
  r.w.ax = 0x4f01;
  r.w.cx = mode;
  if (!VBE_buffer_call(&r, vbe_mode_info, sizeof(*vbe_mode_info))) {
    return 0;
  }
  return vbe_mode_info->mode_attributes & VBE_MODE_AVAILABLE;
}

int VBE_get_pmode_funcs(VBEPmodeFuncs *vbe_pmode_funcs) {
  RMREGS r;
  memset(&r, 0, sizeof(r));
  r.w.ax = 0x4f0a;
  r.h.bl = 0;
  DPMI_int86(0x10, &r);
  memset(vbe_pmode_funcs, 0, sizeof(*vbe_pmode_funcs));
  if (r.w.ax == 0x4f) {
    char *f;
    short *p;
    f = CONV_RM_TO_PM((r.es << 16) + r.w.di);
    p = (short *)f;
    vbe_pmode_funcs->set_window_func = f + p[0];
    vbe_pmode_funcs->set_display_start_func = f + p[1];
    vbe_pmode_funcs->set_palette_func = f + p[2];
  }
  return r.w.ax == 0x4f;
}

int VBE_set_mode(int mode) {
  RMREGS r;
  memset(&r, 0, sizeof(r));
  r.w.ax = 0x4f02;
  r.w.bx = mode;
  DPMI_int86(0x10, &r);
  return r.w.ax == 0x4f;
}

unsigned char far *VBE_get_lfb(int physical_base_ptr, int total_pages) {
  int sel;
  int limit;
  int addr;
  int err;
  limit = total_pages*64*1024 - 1;
  addr = DPMI_map_physical_address(physical_base_ptr, limit);
  if (addr == 0) {
    return NULL;
  }
  sel = DPMI_alloc_sel();
  if (sel == 0) {
    return NULL;
  }
  err = 0;
  if (limit >= 0x10000) err = !DPMI_set_selector_rights(sel, 0x8092);
  if (!err) err = !DPMI_set_selector_base(sel, addr);
  if (!err) err = !DPMI_set_selector_limit(sel, limit);
  if (err) {
    DPMI_free_sel(sel);
    return NULL;
  }
  return MK_FP(sel, 0);
}

const VBEInfo *VBE_init() {
  if (VBE_get_info(&vbe_info)) {
    if (vbe_info.vbe_version >= 0x200) {
      VBE_get_pmode_funcs(&vbe_pmode_funcs);
    }
    return &vbe_info;
  }
  return NULL;
}

const VBEModeInfo *VBE_init_mode(int mode) {
  if (VBE_get_mode_info(&vbe_mode_info, mode)) {
    return &vbe_mode_info;
  }
  return NULL;
}
