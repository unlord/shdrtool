#if !defined(_vbe_H)
# define _vbe_H (1)

#define VBE_INFO_8BIT_DAC       0x0001  /* DAC can be switched to 8-bit mode */
#define VBE_INFO_VSYNC_PAL      0x0004  /* programmed DAC with blank bit */

typedef struct VBEInfo VBEInfo;

#pragma pack(1)
struct VBEInfo {
  char vbe_signature[4];       /* 'VESA' 4 byte signature */
  short vbe_version;           /* VBE version number */
  char *oem_string_ptr;        /* pointer to OEM string */
  int capabilities;            /* capabilities of video card */
  short *video_mode_ptr;       /* pointer to video modes */
  short total_memory;          /* number of 64k memory pages */

  /* VBE 2.0 extensions */
  short oem_software_rev;      /* OEM software revision number */
  char *oem_vendor_name_ptr;   /* pointer to vendor name string */
  char *oem_product_name_ptr;  /* pointer to product name string */
  char *oem_product_rev_ptr;   /* pointer to product revision string */
  char reserved[222];          /* pad to 256 byte block size */
  char oem_data[256];          /* scratch space for OEM data */
};

#define VBE_MODE_AVAILABLE      0x0001  /* video mode is available */
#define VBE_MODE_COLOR_MODE     0x0008  /* mode is a color video mode */
#define VBE_MODE_GRAPHICS_MODE  0x0010  /* mode is a graphics mode */
#define VBE_MODE_NON_BANKED     0x0040  /* banked mode is not supported */
#define VBE_MODE_LINEAR         0x0080  /* linear mode supported */

#define VBE_USE_LFB             0x4000  /* enable linear frame buffer */

typedef struct VBEModeInfo VBEModeInfo;

struct VBEModeInfo {
  short mode_attributes;       /* mode attributes */
  char window_a_attributes;    /* window A attributes */
  char window_b_attributes;    /* window B attributes */
  short window_granularity;    /* window granularity kB */
  short window_size;           /* window size kB */
  short window_a_segment;      /* window A segment */
  short window_b_segment;      /* window B segment */
  void (*win_func_ptr)(int p); /* pointer to window function */
  short bytes_per_scanline;    /* bytes per scanline */
  short pixel_width;           /* horizontal resolution */
  short pixel_height;          /* vertical resolution */
  char x_char_size;            /* character cell width */
  char y_char_size;            /* character cell height */
  char number_of_planes;       /* number of memory planes */
  char bits_per_pixel;         /* bits per pixel */
  char number_of_banks;        /* number of CGA style banks */
  char memory_model;           /* memory model type */
  char bank_size;              /* size of CGA style banks */
  char number_of_image_pages;  /* number of image pages */
  char reserved;               /* reserved */

  /* VBE 1.2 */
  char red_mask_size;          /* size of direct color red mask */
  char red_field_position;     /* bit position of red mask LSB */
  char green_mask_size;        /* size of direct color green mask */
  char green_field_position;   /* bit position of green mask LSB */
  char blue_mask_size;         /* size of direct color blue mask */
  char blue_field_position;    /* bit position of blue mask LSB */
  char reserve_mask_size;      /* size of direct color blue mask */
  char reserve_field_position; /* bit position of blue mask LSB */
  char direct_color_mode_info; /* direct color mode attributes */

  /* VBE 2.0 */
  int physical_base_ptr;       /* physical address for linear frame buffer */
  int off_screen_mem_offset;   /* pointer to start of off secreen memory */
  short off_screen_mem_size;   /* amount of off screen memory in 1 kB units */
  char reserved2[206];         /* pad to 256 byte block size */
};

typedef struct VBEPmodeFuncs VBEPmodeFuncs;

struct VBEPmodeFuncs {
  void *set_window_func;
  void *set_display_start_func;
  void *set_palette_func;
};

typedef struct VBEColor VBEColor;

struct VBEColor {
  union {
    struct {
      unsigned char r;
      unsigned char g;
      unsigned char b;
      unsigned char a;
    };
    unsigned char p[4];
    unsigned int v;
  };
};

int VBE_set_window(int addr);
int VBE_set_display_start(int row, int col);
int VBE_set_palette(const VBEColor *colors, short start, short num);
int VBE_get_info(VBEInfo *vbe_info);
int VBE_get_mode_info(VBEModeInfo *vbe_mode_info, int mode);
int VBE_get_pmode_funcs(VBEPmodeFuncs *vbe_pmode_funcs);
int VBE_set_mode(int mode);
unsigned char far *VBE_get_lfb(int physical_base_ptr, int total_pages);
const VBEInfo *VBE_init();
const VBEModeInfo *VBE_init_mode(int mode);

#endif
