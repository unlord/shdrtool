#include "kb.h"
#include "vbe.h"

#pragma aux main modify [ eax ebx ecx edx esi edi ebp ]
void main() {
  /* Set 640x480 24bpp video mode */
  VBE_set_mode(0x112);
  do {
    /* */
  }
  while (!KB_esc());
  /* Return to text mode */
  VBE_set_mode(0x3);
}
